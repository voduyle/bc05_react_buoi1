import React, { Component } from 'react'

export default class Footer extends Component {
    render() {
        return (
            <footer>
                <p className='py-5 mb-0'>Copyright © Your Website 2022</p>
            </footer>
        )
    }
}
