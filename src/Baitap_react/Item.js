import React, { Component } from 'react'

export default class Item extends Component {
    render() {
        return (
            <div className='item'>
                <div className="item__bg mb-5 px-5">
                    <i class="fa fa-code icon"></i>
                    <h3>Simple clean code</h3>
                    <p>We keep our dependencies up to date and squash bugs as they come!</p>
                </div>
            </div>
        )
    }
}
