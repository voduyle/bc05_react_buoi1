import logo from './logo.svg';
import './App.css';
import Header from './Baitap_react/Header';
import Banner from './Baitap_react/Banner';
import Item from './Baitap_react/Item';
import Footer from './Baitap_react/Footer';

function App() {
  return (
    <div className="App">
      <div id='header'>
        <Header />
      </div>
      <div id="banner">
        <Banner />
      </div>
      <div id="item">
        <div className="container px-0">
          <div className="row">
            <div className="col-12 col-lg-6">
              <Item />
            </div>
            <div className="col-12 col-lg-6">
              <Item />
            </div>
            <div className="col-12 col-lg-6">
              <Item />
            </div>
            <div className="col-12 col-lg-6">
              <Item />
            </div>
            <div className="col-12 col-lg-6">
              <Item />
            </div>
            <div className="col-12 col-lg-6">
              <Item />
            </div>
          </div>
        </div>
      </div>
      <div className='footer'>
        <Footer />
      </div>
    </div>
  );
}

export default App;
